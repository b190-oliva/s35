
const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3000;

//SECTION - MongoDB connection
//connect to the data by passing in connection string(from mongoDB)
// ".connect()" lets us connect and access the database that is sent to it via string
// "b190-to-do" modification is the database we have created in our MongoDB
mongoose.connect("mongodb+srv://39dexshaman:Chowking123@wdc028-course-booking.8f4g3cd.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

//schema() is a method inside the mongoose that let us create a schema for our database, it receives an object with properties and data types for each property they have
const taskSchema = new mongoose.Schema({
	name:String,
	status: {
		type: String,
		default: "pending"
	}
});

//SECTION - Models
// models in mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// models use schemas and they act as the middleman from the server to the database
//First parameter is the collection in where to store the data
//Second parameter is used to specify the Schema /blueprint of the documents that will be stored in the MongoDB collection.
//  models must be written in singular form, sentence case
// the "Task" variable will be used to run commands for interracting with our database

const Task = mongoose.model("Task", taskSchema);


app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/tasks",(req,res)=>{
	//"findOne" is the mongoose method for finding documents in the database just like .find() in robo3t (mongoDB)
	Task.findOne({name:req.body.name},(err,result)=>{
		if(result!==null && result.name === req.body.name)
		{
			return res.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created");
				};
			});
		}
	});
});

app.get("/allTasks",(req,res)=>{
	Task.find({}, (err,result)=> {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json(result);
			// data : result
		};
	});
});

// ===Activity===

/*
	Business logic for sign up
	- add a functionality to check there are duplicate users
	- if the user is already existing, return an error
	- if the user is not existing, save/add the user in the database
	- the user will be coming from the request's body.
	- create a new User object w/ a "username" and "password" field/property
*/

// User Schema

const userSchema = new mongoose.Schema({
	userName:String,
	passWord: String
});

const User = mongoose.model("User", userSchema);

// POST request for new user to sign up

app.post("/signup",(req,res)=>{
	User.findOne({userName:req.body.userName},(err,result)=>{
		if(result!==null && result.userName === req.body.userName)
		{
			return res.send("profile already exists!");
		}
		else{
			let newUser = new User({
				userName: req.body.userName,
				passWord: req.body.passWord
			});
			newUser.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(200).send("Succesfully signed up!");
				};
			});
		}
	});
});

app.listen(port,()=> console.log(`Server running at ${port}`));




